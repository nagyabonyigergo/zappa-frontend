import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { EMAIL_REGEX } from './../../shared/regex/regex';
import { AuthService } from '../../services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from '../snackbar/snackbar.component';

@Component({
  selector: 'app-registration-dialog',
  templateUrl: './registration-dialog.component.html',
  styleUrls: ['./registration-dialog.component.scss'],
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  errorMessage: string;

  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.reset();
  }

  reset() {
    this.registrationForm = new FormGroup(
      {
        username: new FormControl('', [Validators.minLength(3), Validators.required]),
        email: new FormControl('', [Validators.pattern(EMAIL_REGEX), Validators.required]),
        password: new FormControl('',[Validators.minLength(5), Validators.required]),
        validation: new FormControl('', [Validators.required]),
      },
      {
        validators: this.passwordConfirming,
      }
    );
  }

  registerNewUser() {
    if(!this.registrationForm.invalid){
      this.authService
      .createUser(
        this.registrationForm.value.email,
        this.registrationForm.value.password,
        this.registrationForm.value.username
      )
      .then(async (res) => {
        this.dialog.closeAll();
        await this.snackBar.openFromComponent(SnackbarComponent, {
          duration: 3 * 1000,
          data: {
            isSuccess: true,
            message: res.message
          }
        });
      })
      .catch((error) => {
        this.errorMessage = error.error.message;
      });
    }
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('password').value !== c.get('validation').value) {
      return { invalid: true };
    }
  }
}
