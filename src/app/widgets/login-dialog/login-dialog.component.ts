import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss'],
})
export class LoginDialogComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: string;

  @HostListener('document:keydown.enter') onKeydownHandler() {
    this.login();
  }

  constructor(private authService: AuthService, private dialog: MatDialog) {}

  ngOnInit() {
    this.reset();
  }

  reset() {
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
      isRememberMe: new FormControl(false),
    });
  }

  login() {
    return this.authService
      .login(this.loginForm.value.email, this.loginForm.value.password)
      .then(res => {
        if(res.status === 401){
          this.errorMessage = res.error.message;
          return false;
        }
        this.dialog.closeAll();
      })
  }
}
