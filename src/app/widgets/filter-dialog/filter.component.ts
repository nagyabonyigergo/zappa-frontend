import { Component, OnInit, Inject, Optional } from '@angular/core';
import { JobFreelanceCardModel } from 'src/app/shared/models/job-freelance-card.model';
import * as subCategories from '../../shared/sub-categories/sub-categories';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export const JOBS: JobFreelanceCardModel[] = [
  {
    imageSrc: '../assets/design.jpg',
    title: 'Graphics & Design',
    jobCategories: [
      'Graphics & Design',
      'graphics',
      subCategories.DESING_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/food.jpg',
    title: 'Food',
    jobCategories: ['Food', 'food', subCategories.FOOD_CATREGORIES],
  },
  {
    imageSrc: '../assets/odd.jpg',
    title: 'Odd Jobs',
    jobCategories: ['Odd Jobs', 'oddjobs', subCategories.ODD_JOBS_CATREGORIES],
  },
  {
    imageSrc: '../assets/copywriter.jpg',
    title: 'Copywriter',
    jobCategories: [
      'Copywriter',
      'copywriter',
      subCategories.COPYWRITER_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/lifestyle.jpg',
    title: 'Lifestyle',
    jobCategories: [
      'Lifestyle',
      'lifestyle',
      subCategories.LIFESTYLE_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/video.jpg',
    title: 'Video & Animation',
    jobCategories: [
      'Video & Animation',
      'videoanimation',
      subCategories.VIDEO_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/it.jpg',
    title: 'IT',
    jobCategories: ['IT', 'it', subCategories.IT_CATREGORIES],
  },
  {
    imageSrc: '../assets/music.jpg',
    title: 'Music & Audio',
    jobCategories: [
      'Music & Audio',
      'musicaudio',
      subCategories.MUSIC_CATREGORIES,
    ],
  },
];

export const PRICES_TO_FILTER: any[] = [
  { label: 'Lower than 50', value: 50 },
  { label: 'Higher than 50', value: 51 },
];

export const FREELANCER_EMPLOYER_FILTER: any[] = [
  { label: 'Freelancer', value: true },
  { label: 'Employer', value: false },
];

export const UPLOADED_FILTER: any[] = [
  { label: 'Today', value: 25 },
  { label: 'This week', value: 50 },
  { label: 'This month', value: 51 },
  { label: 'Latest', value: 51 },
  { label: 'This month', value: 51 },
];

@Component({
  selector: "filter",
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  jobs = JOBS;
  priceToFilter = PRICES_TO_FILTER;
  uploadedFilter = UPLOADED_FILTER;
  freelancerEmpolyerFilter = FREELANCER_EMPLOYER_FILTER;

  isFreelancer: any;
  price: number;
  date: any;
  category: any;
  specificJob: any;
  title: any;
  removable = true;

  constructor(
    public dialogRef: MatDialogRef<FilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.isFreelancer = this.data.isFreelancer;
    this.specificJob = this.data.specificJob;
    this.title = this.data.title;
    console.log(this.specificJob);
    this.checkIsFilterSelected();
    this.checkIsFreelancer();
  }

  clearFilters() {
    this.price = null;
    this.category = null;
    localStorage.removeItem('category');
    localStorage.removeItem('price');
  }

  changeFilter(value, filterType){
    filterType === 'category' ? localStorage.setItem('category', value) : localStorage.setItem('price', value.toString());
  }

  checkIsFilterSelected(){
    this.price = parseInt(localStorage.getItem('price'), 10 );
    this.category = localStorage.getItem('category');
  }
  
  checkIsFreelancer() {
    this.isFreelancer = localStorage.getItem('isFreelancer');
    if (this.isFreelancer != null) {
      this.isFreelancer === 'true'
        ? (this.isFreelancer = true)
        : (this.isFreelancer = false);
    }
  }

  changeIsFreelancer(isFreelancer) {
    localStorage.setItem('isFreelancer', isFreelancer);
  }

  removeSpecificJob() {
    const jobIndex = this.data.jobIndex;
    this.specificJob = null;
    localStorage.setItem('specificJobIndex', jobIndex);
    this.clearFilters();
  }

  setFilter(){
    const filter = {price: this.price, category: this.category, isFreelancer: this.isFreelancer}
    this.dialogRef.close({ filters: filter, specificJob: this.specificJob});
  }
}
