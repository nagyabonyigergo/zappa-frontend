import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-job-choice',
  templateUrl: './job-choice.component.html',
  styleUrls: ['./job-choice.component.scss']
})
export class JobChoiceComponent implements OnInit {

  isFreelance: boolean = null;

  constructor(public dialogRef: MatDialogRef<JobChoiceComponent>) { }

  ngOnInit() {
  }

  showData(isFreelancer) {
    this.dialogRef.close({ event: isFreelancer })
  }

}
