import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobChoiceComponent } from './job-choice.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [JobChoiceComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    FormsModule
  ],
  exports: [JobChoiceComponent]
})
export class JobChoiceModule { }
