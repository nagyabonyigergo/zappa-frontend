import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models/user.model';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user:User = undefined ;

  userData = {
    name: undefined,
    profile: true
  }

  constructor(private authService: AuthService) { }

  ngOnInit() {
    localStorage.removeItem('isFreelancer');
    localStorage.removeItem('specificJobIndex');
    this.loadUser();
  }

  loadUser(){
    this.authService.currentUser.subscribe( user => {
      this.user = user
      this.userData.name = this.user.username;
    });
  }
}
