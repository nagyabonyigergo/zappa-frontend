import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { Routes, RouterModule } from '@angular/router';
import { MonoAvatarModule } from 'src/app/shared/directives/mono-avatar/mono-avatar.module';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  },
];


@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MonoAvatarModule,
    MatCardModule,
    MatDividerModule
  ]
})
export class ProfileModule { }
