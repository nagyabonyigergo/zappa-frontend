import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestListComponent } from './request-list.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule, Routes } from '@angular/router';
import { MonoAvatarModule } from 'src/app/shared/directives/mono-avatar/mono-avatar.module';
import { NoDataModule } from 'src/app/shared/no-data/no-data.module';

const routes: Routes = [{ path: '', component: RequestListComponent }];

@NgModule({
  declarations: [RequestListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatIconModule,
    MonoAvatarModule,
    NoDataModule
  ],
})
export class RequestListModule {}
