import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss']
})
export class RequestListComponent implements OnInit {

  requests = [];
  noRequest = {
    icon: 'email',
    text: 'Currently You Have No Requests'
  }

  constructor(private requestService: RequestService, private route: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
   this.loadRequests();
  }

  loadRequests(){
    this.requestService.getRequests()
    .then(requests => {
      this.requests = requests.request;
      this.requests.map(request => {
        request.job.status === false ? 
        request.message = 'Hello, I want to hire you!': 
        request.message =  'Hello, I\'m interested to work with you!'
      });
    }).catch(error => this.errorSnackBar(error)); 
  }

  navigateToRequestDetails(requestId){
    const isWorkingOn = false;
    this.route.navigateByUrl(`/request-details/${requestId};isWorkingOn=${isWorkingOn}`);
  }

  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Sorry something went wrong!',
        },
      });
    }
  }

}
