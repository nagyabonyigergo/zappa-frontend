import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestService } from 'src/app/services/request.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.scss']
})
export class RequestDetailsComponent implements OnInit {
  requestId: any;
  isWorkingOn: boolean;
  requestData: any;

  constructor(private route: ActivatedRoute, private requestService: RequestService, private snackBar: MatSnackBar) { }

  async ngOnInit() {
    this.requestId = this.route.snapshot.paramMap.get('id');
    this.isWorkingOn = JSON.parse(this.route.snapshot.paramMap.get('isWorkingOn'));
    console.log(this.isWorkingOn)
    await this.getJobDetails();
  }

  async getJobDetails() {
    await this.requestService
      .getRequestById(this.requestId)
      .then(result => {
        this.requestData = result.request;
      })
      .catch(error => this.errorSnackBar(error));
  }

  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Sorry something went wrong!',
        },
      });
    }
  }

}
