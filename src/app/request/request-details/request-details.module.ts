import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestDetailsComponent } from './request-details.component';
import { Routes, RouterModule } from '@angular/router';
import { DetailsModule } from 'src/app/shared/details/details.module';


const routes: Routes = [{ path: '', component: RequestDetailsComponent }];

@NgModule({
  declarations: [RequestDetailsComponent],
  imports: [
    CommonModule, 
    RouterModule.forChild(routes),
    DetailsModule 
  ]
})
export class RequestDetailsModule {}
