import { Component, OnInit } from '@angular/core';
import { HomeCardModel } from '../shared/models/home-card.model';
import { Router } from '@angular/router';
import * as subCategories from '../shared/sub-categories/sub-categories';

export const HOME_CARD_CONTENT: HomeCardModel[] = [
  { icon: 'build', title: 'Post Your Skill', label: 'Sell your skills as a freelancer!' },
  {
    icon: 'assignment_turned_in',
    title: 'Hire Freelancer',
    label: 'Post job offer and hire freelancers!',
  },
  { icon: 'done', title: 'Get Jobs Done', label: 'Find your Job or Freelancer and start working!' },
];

export const JOBS: any[] = [
  {
    icon: 'brush',
    title: 'Graphics & Design',
    subCategory: [
      'Graphics & Design',
      'graphics',
      subCategories.DESING_CATREGORIES,
    ],
  },
  {
    icon: 'local_dining',
    title: 'Food',
    subCategory: ['Food', 'food', subCategories.FOOD_CATREGORIES],
  },
  {
    icon: 'format_paint',
    title: 'Odd Jobs',
    subCategory: ['Odd Jobs', 'oddjobs', subCategories.ODD_JOBS_CATREGORIES],
  },
  {
    icon: 'create',
    title: 'Copywriter',
    subCategory: [
      'Copywriter',
      'copywriter',
      subCategories.COPYWRITER_CATREGORIES,
    ],
  },
  {
    icon: 'emoji_food_beverage',
    title: 'Lifestyle',
    subCategory: [
      'Lifestyle',
      'lifestyle',
      subCategories.LIFESTYLE_CATREGORIES,
    ],
  },
  {
    icon: 'movie',
    title: 'Video & Animation',
    subCategory: [
      'Video & Animation',
      'videoanimation',
      subCategories.VIDEO_CATREGORIES,
    ],
  },
  {
    icon: 'computer',
    title: 'IT',
    subCategory: ['IT', 'it', subCategories.IT_CATREGORIES],
  },
  {
    icon: 'mic',
    title: 'Music & Audio',
    subCategory: [
      'Music & Audio',
      'musicaudio',
      subCategories.MUSIC_CATREGORIES,
    ],
  },
];

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {}

  jobs = JOBS;
  homeCardContent = HOME_CARD_CONTENT;

  ngOnInit() {
    this.clearJobSearchInfo();
  }

  navigateToJobs(jobIndex) {
    this.router.navigateByUrl('/jobs-list', { state: { parameter: jobIndex } });
  }

  clearJobSearchInfo(){
    localStorage.removeItem('isFreelancer');
    localStorage.removeItem('specificJobIndex');
  }
}
