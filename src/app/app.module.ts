import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginDialogModule } from './widgets/login-dialog/login-dialog.module';
import { MainNavModule } from './shared/main-nav/main-nav.module';
import { RegistrationModule } from 'src/app/widgets/registration-dialog/registration-dialog.module';
import { JobChoiceModule } from './widgets/job-choice/job-choice.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LoginDialogModule,
    MainNavModule,
    RegistrationModule,
    JobChoiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
