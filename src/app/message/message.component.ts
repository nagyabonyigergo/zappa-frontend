import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';
import { DeleteAlertComponent } from 'src/app/widgets/delete-alert/delete-alert.component';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  messages = [];
  noMessage = {
    icon: 'email',
    text: 'Currently You Have No Messages',
  };

  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.loadMessages();
  }

  loadMessages() {
    this.messageService
      .getMessages()
      .then((result: any) => (this.messages = result.messages))
      .catch((error) => this.errorSnackBar(error));
  }

  deleteMessage(messageId) {
    this.dialog
      .open(DeleteAlertComponent, { data: 'Do you want to delete this job?' })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.messageService
            .deleteMessageById(messageId)
            .then((deletResult: any) => {
              if (deletResult) {
                this.messages = this.messages.filter(
                  (message) => message._id !== messageId
                );
                this.snackBar.openFromComponent(SnackbarComponent, {
                  duration: 3 * 1000,
                  data: {
                    isSuccess: true,
                    message: deletResult.message,
                  },
                });
              }
            })
            .catch((error) => this.errorSnackBar(error));
        }
      });
  }

  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Sorry something went wrong!',
        },
      });
    }
  }
}
