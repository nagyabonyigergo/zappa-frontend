import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageComponent } from './message.component';
import { Routes, RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MonoAvatarModule } from '../shared/directives/mono-avatar/mono-avatar.module';
import { MatButtonModule } from '@angular/material/button';
import { NoDataModule } from '../shared/no-data/no-data.module';
import { DeleteAlertComponent } from '../widgets/delete-alert/delete-alert.component';
import { DeleteAlertModule } from '../widgets/delete-alert/delete-alert.module';

const routes: Routes = [
  {
    path: '',
    component: MessageComponent,
  },
];

@NgModule({
  declarations: [MessageComponent],
  entryComponents: [DeleteAlertComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatIconModule,
    MonoAvatarModule,
    MatButtonModule,
    NoDataModule,
    DeleteAlertModule
  ]
})
export class MessageModule { }
