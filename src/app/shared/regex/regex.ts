export const EMAIL_REGEX =
  '^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})$';

export const NUMBER_REGEX = '^[0-9]*$';
