export interface NavigationModel {
  label: string;
  value?: string;
  icon?: string;
  name: string;
}
