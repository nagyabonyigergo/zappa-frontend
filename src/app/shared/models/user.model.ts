export interface User {
  _id:string,
  email: string,
  username: string,
  imageUrl: string,
  rating: number,
  jobposts: [],
  workingon: []
}