export interface HomeCardModel {
  icon: any,
  title: string;
  label: string;
}
