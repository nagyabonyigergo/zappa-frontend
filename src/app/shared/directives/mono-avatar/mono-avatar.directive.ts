import { Directive, ElementRef, Input, OnInit } from '@angular/core';

const COLORS = [
  '#F44336', '#E91E63', '#9C27B0', ' #673AB7',
  '#3F51B5', '#1E88E5', '#0288D1', '#0097A7',
  '#009688', '#43A047', '#558B2F', '#E65100'];

@Directive({
  selector: '[monoAvatar]'
})
export class MonoAvatarDirective implements OnInit {

  @Input() userData: any;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    if(!this.userData.profile){
        const randomNumber = parseInt(((Math.random() * (COLORS.length - 1)) + 1).toString(), 10);
        const elStyle = this.el.nativeElement.style;
        elStyle.backgroundColor = COLORS[randomNumber];
        elStyle.width = '35px';
        elStyle.height = '35px';
        elStyle.borderRadius = '360px';
        elStyle.verticalAlig = 'middle';
        elStyle.display = 'flex';
        elStyle.justifyContent = 'center';
        elStyle.alignItems = 'center';
        elStyle.cursor = 'default';
        elStyle.color = 'white';
        elStyle.fontSize = '16px'
        this.el.nativeElement.textContent = this.getChars();
    }
    else{
        const randomNumber = parseInt(((Math.random() * (COLORS.length - 1)) + 1).toString(), 10);
        const elStyle = this.el.nativeElement.style;
        elStyle.backgroundColor = COLORS[randomNumber];
        elStyle.width = '150px';
        elStyle.height = '150px';
        elStyle.borderRadius = '360px';
        elStyle.verticalAlig = 'middle';
        elStyle.display = 'flex';
        elStyle.justifyContent = 'center';
        elStyle.alignItems = 'center';
        elStyle.cursor = 'default';
        elStyle.color = 'white';
        elStyle.fontSize = '16px'
        this.el.nativeElement.textContent = this.getChars();
    }
  }

  getChars(): string {
    if (this.userData.name && this.userData.name !== ' ') {
      const tmp = this.userData.name.split(' ');

      let monogram = tmp[0].charAt(0);
      if (tmp.length > 1) {
        monogram += tmp[1].charAt(0);
      } else {
        monogram += tmp[0].charAt(1);
      }
      return monogram.toUpperCase();
    }
    return '?';
  }
}
