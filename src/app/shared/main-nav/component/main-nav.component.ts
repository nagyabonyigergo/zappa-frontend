import { Component, OnInit} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { NavigationModel } from '../../models/navigation.model';

import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from 'src/app/widgets/login-dialog/login-dialog.component';
import { RegistrationComponent } from 'src/app/widgets/registration-dialog/registration-dialog.component';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from '../../models/user.model';
import { couldStartTrivia } from 'typescript';


export interface AssociativeArray {
  [key: string]: string | any;
}

export const NAVIGATION_DATA: NavigationModel[] = [
  { label: 'Jobs', name: 'jobsList' },
  { label: 'Sign In', name: 'signIn' }
];

export const MOBILE_NAVIGATION_DATA: NavigationModel[] = [
  { label: 'Jobs', name: 'jobsList' },
  { label: 'Sign In', name: 'signIn' },
  { label: 'Home', name: 'home'}
];

export const MOBILE_NAVIGATION_DATA_WITH_USER: NavigationModel[] = [
  { label: 'Jobs', name: 'jobsList' },
  { label: 'Post Job', name: 'postJob' },
  { label: 'Messages', name: 'message'},
  { label: 'Requests', name: 'request'},
  { label: 'Manage Jobs', name: 'manageJobs'},
  { label: 'Home', name: 'home'},
  { label: 'Logout', name: 'logout'}
];

export const NAVIGATION_DATA_WITH_USER: NavigationModel[] = [
  { label: 'Jobs', name: 'jobsList' },
  { label: 'Post Job', name: 'postJob'},
];

@Component({
  selector: "main-nav",
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent implements OnInit{
  navigationData;
  mobileNavigationData;
  isAuthorize = false;
  user: User = undefined;
  isMobileSize; 

  userData = {
    name: '',
    profile: false
  }
  
  onClickFunction: AssociativeArray = {
    signIn: () => this.signIn(),
    join: () => this.join(),
    jobsList: () => this.jobsList(),
    logout:() => this.logout(),
    manageJobs: () => this.usersJobPosts(),
    home: () => this.home(),
    postJob: () => this.postJob(),
    message: () => this.message(),
    request: () => this.requestList()
  };
  prevButtonTrigger: any;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private dialog: MatDialog,
    private router: Router,
    private authService: AuthService
  ) {}

  isHandset$: Observable<boolean> = this.breakpointObserver
  .observe(Breakpoints.Handset)
  .pipe(
    map((result) => {
      this.isMobileSize  = result.matches;
      return result.matches}),
    shareReplay()
  );
  
  
  ngOnInit(){
   this.getUser();
  }


  getUser(){
    this.authService.currentUser.subscribe( result => {
      if(!result){
        this.user = undefined;
      }
      else{
        this.user = result;
        this.userData.name = this.user.username;
      }
      this.user ? this.navigationData = NAVIGATION_DATA_WITH_USER : this.navigationData = NAVIGATION_DATA;
      this.user ? this.mobileNavigationData = MOBILE_NAVIGATION_DATA_WITH_USER : this.mobileNavigationData = MOBILE_NAVIGATION_DATA;
    })
  }

  chooseFunction(functionName) {
    this.onClickFunction[functionName]();
  }

  signIn() {
    this.dialog.open(LoginDialogComponent, { panelClass: 'login-dialog'});
  }

  join() {
    this.dialog.open(RegistrationComponent,  { panelClass: 'registration-dialog'});
  }

  jobsList() {
    this.router.navigateByUrl('/jobs-list');
  }

  logout(){
    this.authService.logout();
  }

  navigateToProfile(){
    this.router.navigateByUrl('/profile');
  }

  usersJobPosts(){
    this.router.navigateByUrl('/manage-jobs');
  }

  postJob(){
    this.router.navigateByUrl('/add-job');
  }

  home(){
    this.router.navigateByUrl('/home');
  }

  message(){
    this.router.navigateByUrl('/message');
  }

  requestList(){
    this.router.navigateByUrl('/request-list');
  }
}
