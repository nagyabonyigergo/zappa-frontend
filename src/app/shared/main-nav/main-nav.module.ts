import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainNavComponent } from './component/main-nav.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LayoutModule } from '@angular/cdk/layout';
import { MatDialogModule } from '@angular/material/dialog';
import { LoginDialogComponent } from 'src/app/widgets/login-dialog/login-dialog.component';
import { RegistrationComponent } from 'src/app/widgets/registration-dialog/registration-dialog.component';
import { RouterModule } from '@angular/router';
import { MonoAvatarModule } from 'src/app/shared/directives/mono-avatar/mono-avatar.module';
import { MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';

@NgModule({
  declarations: [MainNavComponent],
  entryComponents: [LoginDialogComponent, RegistrationComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    LayoutModule,
    MatDialogModule,
    RouterModule,
    MonoAvatarModule,
    MatMenuModule,
    MatDividerModule
  ],
  exports: [MainNavComponent],
})
export class MainNavModule {}
