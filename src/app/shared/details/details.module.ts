import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from './details.component';
import { MonoAvatarModule } from '../directives/mono-avatar/mono-avatar.module';
import { DeleteAlertComponent } from 'src/app/widgets/delete-alert/delete-alert.component';
import { DeleteAlertModule } from 'src/app/widgets/delete-alert/delete-alert.module';
import { MatButtonModule } from '@angular/material/button';
import { LoginDialogComponent } from 'src/app/widgets/login-dialog/login-dialog.component';
import { LoginDialogModule } from 'src/app/widgets/login-dialog/login-dialog.module';


@NgModule({
  declarations: [DetailsComponent],
  entryComponents: [DeleteAlertComponent, LoginDialogComponent],
  imports: [
    CommonModule,
    MonoAvatarModule,
    DeleteAlertModule,
    MatButtonModule,
    LoginDialogModule
  ],
  exports: [DetailsComponent]
})
export class DetailsModule { }
