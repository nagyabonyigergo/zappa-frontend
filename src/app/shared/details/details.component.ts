import { Component, OnInit, Input } from '@angular/core';
import { User } from '../models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { RequestService } from 'src/app/services/request.service';
import { DeleteAlertComponent } from 'src/app/widgets/delete-alert/delete-alert.component';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessageService } from 'src/app/services/message.service';
import { Router } from '@angular/router';
import { JobService } from 'src/app/services/job.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  @Input() contentDetails: any;
  @Input() isWorkingOn: boolean;

  jobDetails: any;
  creator: any;
  isFreelancer: boolean;
  isCurrentUsersProduct = false;
  isWorkingOnDetails: boolean;
  message: string;
  user: User;
  image: any;
  userData = {
    name: '',
    profile: false,
  };

  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private requestService: RequestService,
    private messageService: MessageService,
    private snackBar: MatSnackBar,
    private router: Router,
    private jobService: JobService
  ) {}

  async ngOnInit() {
    this.jobDetails = this.contentDetails.job;
    this.creator = this.contentDetails.creator;
    this.image = this.jobDetails.imageUrl;
    await this.loadUser();
  }

  async loadUser() {
    await this.authService.currentUser.subscribe((user) => {
      if (user) {
        this.user = user;
        this.userData.name = this.jobDetails.creator.username;
        this.checkIsCurrentUsersProduct();
        this.createMessage();
      }
    });
  }

  checkIsCurrentUsersProduct() {
    if (this.user) {
      this.user._id === this.jobDetails.creator._id
        ? (this.isCurrentUsersProduct = true)
        : (this.isCurrentUsersProduct = false);
    } else {
      this.isCurrentUsersProduct = false;
    }
  }

  createMessage() {
    if (this.creator) {
      this.jobDetails.status === false
        ? (this.message = `Hello, I want to hire you! My email is 
            ${this.creator.email}. Contact me please.`)
        : (this.message = `Hello, I\'m interested to work with you! My email is
            ${this.creator.email}. Contact me please.`);
    }
  }

  deleteJob() {
    this.dialog
      .open(DeleteAlertComponent, { data: 'Do you want to delete this job?' })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.jobService
            .deleteJobById(this.jobDetails._id)
            .then(async (res: any) => {
              if (res.message) {
                await this.snackBar.openFromComponent(SnackbarComponent, {
                  duration: 3 * 1000,
                  data: {
                    isSuccess: true,
                    message: res.message,
                  },
                });
                this.router.navigateByUrl('/manage-jobs');
              }
            })
            .catch((error) => this.errorSnackBar(error));
        }
      });
  }

  createRequest() {
    if (this.user) {
      this.requestService
        .createRequest(this.jobDetails._id)
        .then((result) => {
          if (result.message) {
            this.snackBar.openFromComponent(SnackbarComponent, {
              duration: 3 * 1000,
              data: {
                isSuccess: true,
                message: result.message,
              },
            });
          }
        })
        .catch((error) => {
          this.errorSnackBar(error);
        });
    } else {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Login for Enroll!',
        },
      });
    }
  }

  acceptRequest() {
    const message = `Request for ${this.jobDetails.jobTitle} is accepted`;
    const requestId = this.contentDetails._id;
    const requestCreatorId = this.creator._id;
    this.messageService
      .createMessage(message, requestId, requestCreatorId)
      .then((result) => {
        this.succesSnackBar(result);
      })
      .catch((error) => {
        this.errorSnackBar(error);
      });
  }

  declineRequest() {
    const message = `Sorry Your request for ${this.jobDetails.jobTitle} is declined.`;
    const requestId = this.contentDetails._id;
    const requestCreatorId = this.creator._id;
    this.dialog
      .open(DeleteAlertComponent, {
        data: 'Do you want to decline this request?',
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.messageService
            .createMessage(message, requestId, requestCreatorId)
            .then((createMessageResult) => {
              this.requestService.deleteRequestById(requestId);
              this.succesSnackBar(createMessageResult);
            })
            .catch((error) => this.errorSnackBar(error));
        }
      });
  }

  finishJob(){
    const message = `Your request for ${this.jobDetails.jobTitle} is finished.`;
    const requestId = this.contentDetails._id;
    const requestCreatorId = this.creator._id;
    this.requestService.deleteRequestById(requestId).then( result => {
      this.messageService
      .createMessage(message, requestId, requestCreatorId)
      .then( messageCreateResult => {
        if (messageCreateResult.message) {
          this.snackBar.openFromComponent(SnackbarComponent, {
            duration: 3 * 1000,
            data: {
              isSuccess: true,
              message: messageCreateResult.message,
            },
          });
          this.router.navigateByUrl('/manage-jobs');
        }
      })
      .catch((error) => {
        this.errorSnackBar(error);
      });
    }).catch(error => this.errorSnackBar(error));
  }

  succesSnackBar(result) {
    if (result.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: result.message,
        },
      });
    }
    this.router.navigateByUrl('/request-list');
  }

  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: error.error.message || 'Sorry something went wrong',
        },
      });
    }
  }
}
