import * as subCategories from './../../shared/sub-categories/sub-categories';
import { JobFreelanceCardModel } from 'src/app/shared/models/job-freelance-card.model';

export const JOBS: JobFreelanceCardModel[] = [
  {
    imageSrc: '../assets/design.jpg',
    title: 'Graphics & Design',
    jobCategories: [
      'Graphics & Design',
      'graphics',
      subCategories.DESING_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/food.jpg',
    title: 'Food',
    jobCategories: ['Food', 'food', subCategories.FOOD_CATREGORIES],
  },
  {
    imageSrc: '../assets/odd.jpg',
    title: 'Odd Jobs',
    jobCategories: ['Odd Jobs', 'oddjobs', subCategories.ODD_JOBS_CATREGORIES],
  },
  {
    imageSrc: '../assets/copywriter.jpg',
    title: 'Copywriter',
    jobCategories: [
      'Copywriter',
      'copywriter',
      subCategories.COPYWRITER_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/lifestyle.jpg',
    title: 'Lifestyle',
    jobCategories: [
      'Lifestyle',
      'lifestyle',
      subCategories.LIFESTYLE_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/video.jpg',
    title: 'Video & Animation',
    jobCategories: [
      'Video & Animation',
      'videoanimation',
      subCategories.VIDEO_CATREGORIES,
    ],
  },
  {
    imageSrc: '../assets/it.jpg',
    title: 'IT',
    jobCategories: ['IT', 'it', subCategories.IT_CATREGORIES],
  },
  {
    imageSrc: '../assets/music.jpg',
    title: 'Music & Audio',
    jobCategories: [
      'Music & Audio',
      'musicaudio',
      subCategories.MUSIC_CATREGORIES,
    ],
  },
];
