import { Component, OnInit, Input } from '@angular/core';
import { NavigationModel } from '../../models/navigation.model';


@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor() { }

  @Input() navigationData: NavigationModel[];

  ngOnInit() {

  }

  showData(url: string) {
  }

}
