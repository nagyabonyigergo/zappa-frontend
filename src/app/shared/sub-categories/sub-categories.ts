export const DESING_CATREGORIES: any[] = [
  { label: 'Logo design', value: 'logo' },
  { label: 'Web design', value: 'web' },
  { label: 'Poster design', value: 'poster' },
]

export const FOOD_CATREGORIES: any[] = [
  { label: 'Wedding cook', value: 'wedding' },
  { label: 'Party cook', value: 'party' },
  { label: 'Baker', value: 'baker' },
]

export const ODD_JOBS_CATREGORIES: any[] = [
  { label: 'Electrician', value: 'electrician' },
  { label: 'Gardener', value: 'gardener' },
  { label: 'Plumber', value: 'plumber' },
]

export const COPYWRITER_CATREGORIES: any[] = [
  { label: 'Resume writing', value: 'resume' },
  { label: 'Translation', value: 'translation' },
  { label: 'Cover letters', value: 'letter' },
]

export const LIFESTYLE_CATREGORIES: any[] = [
  { label: 'Gaming', value: 'game' },
  { label: 'Traveling', value: 'travel' },
  { label: 'Collectibles', value: 'collectible' },
]
export const VIDEO_CATREGORIES: any[] = [
  { label: 'Photographer', value: 'photo' },
  { label: 'Videographer', value: 'video' },
  { label: 'Editor', value: 'edit' },
]

export const IT_CATREGORIES: any[] = [
  { label: 'Web developer', value: 'webdev' },
  { label: 'Mobile Apps', value: 'mobileapp' },
  { label: 'IT specialist', value: 'itspecialist' },
]

export const MUSIC_CATREGORIES: any[] = [
  { label: 'Voice over', value: 'voiceover' },
  { label: 'Songwritters', value: 'songwritter' },
  { label: 'Sound effects', value: 'soundeffect' },
]