import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/guards/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', loadChildren: './home/home.module#HomeModule' },
  { path: 'jobs-list', loadChildren: './jobs/jobs-list/jobs-list.module#JobsListModule' },
  { path: 'job-details/:id', loadChildren: './jobs/job-details/job-details.module#JobDetailsModule' },
  { path: 'manage-jobs', loadChildren: './jobs/manage-jobs/manage-jobs.module#ManageJobsModule', canActivateChild: [AuthGuard] },
  { path: 'add-job', loadChildren: './jobs/add-job/add-job.module#AddJobModule', canActivateChild: [AuthGuard] },
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule', canActivateChild: [AuthGuard] },
  { path: 'message', loadChildren: './message/message.module#MessageModule', canActivateChild: [AuthGuard] },
  { path: 'request-list', loadChildren: './request/request-list/request-list.module#RequestListModule', canActivateChild: [AuthGuard] },
  { path: 'request-details/:id', loadChildren: './request/request-details/request-details.module#RequestDetailsModule', canActivateChild: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
