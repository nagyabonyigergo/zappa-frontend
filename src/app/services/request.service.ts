import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../shared/models/user.model';

const BACKEND_URL = environment.apiUrl;

@Injectable({ providedIn: 'root' })
export class RequestService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  getRequests() {
    const token = localStorage.getItem('token');
    return this.httpClient
      .get(BACKEND_URL + '/request/getrequests', {
        headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
      })
      .toPromise()
      .then(request => request)
      .catch(error => error);
  }

  createRequest(jobId: string) {
    const token = localStorage.getItem('token');
    const requestData = { jobId};
    return this.httpClient
      .post<any>(BACKEND_URL + '/request/createrequest', requestData, {
        headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
      })
      .toPromise()
      .then(res => res)
      .catch(error => {throw error});
  }

  getRequestById(id){
    return this.httpClient.get(BACKEND_URL + '/request/getrequestbyid/'+ id)
    .toPromise()
    .then(request => request ).catch(error => error);
  }

  deleteRequestById(id) {
    const token = localStorage.getItem('token');
    return this.httpClient
      .delete(BACKEND_URL + '/request/deleterequest/' + id, {
        headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
      })
      .toPromise()
      .then(result => result)
      .catch(error => {throw error});
  }
}
