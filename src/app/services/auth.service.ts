import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../shared/models/user.model';
import { BehaviorSubject, Observable, observable } from 'rxjs';
import { Router } from '@angular/router';

const BACKEND_URL = environment.apiUrl;

@Injectable({ providedIn: 'root' })
export class AuthService {
  private isAuthenticated = false;
  private user: BehaviorSubject<User> = new BehaviorSubject<any>(undefined);
  currentUser = this.user.asObservable();
  private token: string;
  private tokenTimer: any;

  constructor(private httpClient: HttpClient, private router: Router) {}

  createUser(email: string, password: string, username: string): Promise<any> {
    // egyelore dummy status
    const authData = { email, password, username};
    return this.httpClient
      .put(BACKEND_URL + '/auth/signup', authData, {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      })
      .toPromise();
  }

  async login(email: string, password: string): Promise<any> {
    const authData = { email, password };
    return await this.httpClient
      .post<any>(BACKEND_URL + '/auth/login', authData)
      .toPromise()
      .then( res => {
          this.user.next(res.user)
          this.token = res.token;
          if(this.token){
            this.setAuthTimer(res.expiresIn);
            this.isAuthenticated = true;
            const expirationDate = new Date(new Date().getTime() + res.expiresIn * 1000);
            this.saveAuthData(expirationDate);
            return res;
          }
      })
      .catch((error) => {
        return error;
      });
  }
 
  logout(){
    this.token = null;
    this.isAuthenticated = false;
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigateByUrl('/home');
    this.user.next(undefined);
  }

  private setAuthTimer(duration: number){
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(expirationDate: Date){
    localStorage.setItem('token', this.token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    this.currentUser.subscribe( user => {
      localStorage.setItem('user', JSON.stringify(user))}); 
  }

  private clearAuthData(){
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('user');
  }

  autoAuthUser(){
    const authInformation = this.getAuthData();
    if(!authInformation){
      return;
    }
    const expiresIn = authInformation.expirationDate.getTime() - new Date().getTime();
    if(expiresIn > 0){
      this.token = authInformation.token;
      this.user.next(authInformation.user)
      this.isAuthenticated = true;
      this.setAuthTimer(expiresIn / 1000);
    }
  }

  private getAuthData(){
    try{
      const user: User = JSON.parse(localStorage.getItem('user'));
      const token = localStorage.getItem('token');
      const expirationDate = localStorage.getItem('expiration');
      if(token || expirationDate || user){
        return{ user, token , expirationDate: new Date(expirationDate) };
      }
    }catch(err){
      return;
    }
  }

  getIsAuthenticated(): boolean{
    return this.isAuthenticated;
  }
}
