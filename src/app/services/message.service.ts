import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

const BACKEND_URL = environment.apiUrl;

@Injectable({ providedIn: 'root' })
export class MessageService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  getMessages() {
    const token = localStorage.getItem('token');
    return this.httpClient
      .get(BACKEND_URL + '/message/getmessages', {
        headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
      })
      .toPromise()
      .then(request => request)
      .catch(error => {throw error});
  }

  createMessage( message: string, requestId: string, requestCreatorId: string) {
    const token = localStorage.getItem('token');
    const messageData = {message, requestId, requestCreatorId};
    return this.httpClient
      .post<any>(BACKEND_URL + '/message/createmessage', messageData, {
        headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
      })
      .toPromise()
      .then(res => res)
      .catch(error => {throw error});
  }

  deleteMessageById(messageId) {
    const token = localStorage.getItem('token');
    const messageData = {messageId};
    return this.httpClient
      .post(BACKEND_URL + '/message/deletemessage/', messageData,  {
        headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
      })
      .toPromise()
      .then(result => result)
      .catch(error => {throw error});
  }
}
