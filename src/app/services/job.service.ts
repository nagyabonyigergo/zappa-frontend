import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../shared/models/user.model';

const BACKEND_URL = environment.apiUrl;

@Injectable({ providedIn: 'root' })
export class JobService {

  constructor(private httpClient: HttpClient, private router: Router) { }

  loadUserJobPosts() {
    const token = localStorage.getItem('token');
    return this.httpClient.get(BACKEND_URL + '/work/getuserjobandposts', {
      headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
    })
    .toPromise()
    .then(jobPosts => jobPosts ).catch(error =>{throw error});
  }

  createJob(
    title: string,
    description: string,
    status: string,
    category: string,
    subcategory: string,
    jobTitle: string,
    price: number,
    image: string
  ) {
    const token = localStorage.getItem('token');
    const formData = new FormData();
    formData.append('title',title);
    formData.append('description', description);
    formData.append('image', image);
    formData.append('status', status);
    formData.append('price', price.toString());
    formData.append('descrstatusiption', status);
    formData.append('category', category);
    formData.append('subcategory', subcategory);
    formData.append('jobTitle', jobTitle);
    
    return this.httpClient
    .post<any>(BACKEND_URL + '/work/createjob', formData, {
      headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
    })
    .toPromise().then(res => res).catch(error =>{throw error});
  }

  getAllJobs(price, status, subCategory, category) {
    const authData = { price, status, subCategory, category};
    const token = localStorage.getItem('token');
    return this.httpClient.post<any>(BACKEND_URL + '/work/getalljob', authData,  {
      headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
    })
    .toPromise()
    .then(jobPosts => jobPosts ).catch(error => {throw error});
  }

  getJobById(id){
    return this.httpClient.get(BACKEND_URL + '/work/getJob/'+ id)
    .toPromise()
    .then(jobPosts => jobPosts ).catch(error =>{throw error});
  }

  deleteJobById(id) {
    const token = localStorage.getItem('token');
    return this.httpClient.delete(BACKEND_URL + '/work/deletejob/' + id, {
      headers: new HttpHeaders({ Authorization: 'Bearer ' + token }),
    })
    .toPromise()
    .then(jobPosts => jobPosts ).catch(error => {throw error});
  }
}
