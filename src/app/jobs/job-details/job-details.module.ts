import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobDetailsComponent } from './job-details.component';
import { Routes, RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MonoAvatarModule } from 'src/app/shared/directives/mono-avatar/mono-avatar.module';
import { DetailsModule } from 'src/app/shared/details/details.module';

const routes: Routes = [{ path: '', component: JobDetailsComponent }];

@NgModule({
  declarations: [JobDetailsComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MonoAvatarModule,
    DetailsModule,
    RouterModule.forChild(routes),
  ],
  exports: [JobDetailsComponent],
})
export class JobDetailsModule {}
