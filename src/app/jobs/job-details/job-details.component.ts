import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobService } from 'src/app/services/job.service';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss'],
})
export class JobDetailsComponent implements OnInit {
  jobDetails: any;
  jobId: string;
 

  constructor(
    private route: ActivatedRoute,
    private jobService: JobService,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    this.jobId = this.route.snapshot.paramMap.get('id');
    await this.getJobDetails();
  }

  async getJobDetails() {
    await this.jobService
      .getJobById(this.jobId)
      .then((result) => {
        this.jobDetails = result;
      })
      .catch((error) => this.errorSnackBar(error));
  }

  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Sorry something went wrong!',
        },
      });
    }
  }

}
