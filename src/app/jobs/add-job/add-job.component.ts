import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NUMBER_REGEX } from 'src/app/shared/regex/regex';
import { Router, NavigationEnd } from '@angular/router';
import { JobService } from 'src/app/services/job.service';
import { JOBS } from 'src/app/shared/exports/jobs-exports';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.scss'],
})
export class AddJobComponent implements OnInit {
  jobForm: FormGroup;
  selectedFile: File;
  image;
  categories = JOBS;
  subCategories = undefined;
  isDisabled = true;

  constructor(
    private router: Router,
    private jobService: JobService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.clearJobSearchInfo();
    this.reset();
  }

  clearJobSearchInfo(){
    localStorage.removeItem('isFreelancer');
    localStorage.removeItem('specificJobIndex');
  }

  reset() {
    this.jobForm = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.max(150)]),
      description: new FormControl('', [
        Validators.required,
        Validators.max(150),
      ]),
      image: new FormControl('', Validators.required),
      price: new FormControl('', [
        Validators.required,
        Validators.pattern(NUMBER_REGEX),
      ]),
      status: new FormControl(false),
      category: new FormControl('', Validators.required),
      subcategory: new FormControl(
        { value: '', disabled: true },
        Validators.required
      ),
    });
  }

  createJob() {
    if (!this.jobForm.invalid) {
      this.jobService
        .createJob(
          this.jobForm.value.title,
          this.jobForm.value.description,
          this.jobForm.value.status,
          this.jobForm.value.category,
          this.jobForm.value.subcategory.value,
          this.jobForm.value.subcategory.label,
          this.jobForm.value.price,
          this.jobForm.value.image
        )
        .then(async res => {
          if(res.message){
            await this.snackBar.openFromComponent(SnackbarComponent, {
              duration: 3 * 1000,
              data: {
                isSuccess: true,
                message: res.message
              }
            });
            this.router.navigateByUrl('/manage-jobs');
          }
        })
        .catch(error => this.errorSnackBar(error));
    }
  }

  onFileChange(event) {
    const reader = new FileReader();
    this.selectedFile = event.target.files[0];
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.image = reader.result;
    };
    this.jobForm.patchValue({ image: this.selectedFile });
  }

  onCategoryChange(event) {
    this.subCategories = this.categories.filter(
      job => job.jobCategories[1] === event.value
    );
    this.subCategories = this.subCategories[0].jobCategories[2];
    this.jobForm.controls.subcategory.enable();
  }

  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Sorry something went wrong!',
        },
      });
    }
  }
}
