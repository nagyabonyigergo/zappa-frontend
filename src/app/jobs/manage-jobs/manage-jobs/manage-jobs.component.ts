import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { JobService } from 'src/app/services/job.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';

@Component({
  selector: 'app-manage-jobs',
  templateUrl: './manage-jobs.component.html',
  styleUrls: ['./manage-jobs.component.scss'],
})
export class ManageJobsComponent implements OnInit {
  postsAndWorkingon: any;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

    noJob = {
      icon: 'work_off',
      text: 'No Job To Show'
    }

  userJobsAndPosts = [
    { label: 'Posted Jobs', cardContent: [] },
    { label: 'Working On', cardContent: [] },
  ];

  selectedIndex = 0;
  
  constructor(
    private breakpointObserver: BreakpointObserver,
    private jobService: JobService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    localStorage.removeItem('isFreelancer');
    localStorage.removeItem('specificJobIndex');
    this.getUserJobs();
  }

  async getUserJobs() {
    await this.jobService
      .loadUserJobPosts()
      .then((result: any) => {
        this.postsAndWorkingon = result;
        const workingOn = [];
        this.postsAndWorkingon.jobs.map(job => {workingOn.push(job.job)})
        this.userJobsAndPosts = [
          { label: 'Posted Jobs', cardContent: result.posts },
          { label: 'Working On', cardContent: workingOn},
        ];
      })
      .catch(error => this.errorSnackBar(error));
  }

  requestDetails(content) {
    const request = this.postsAndWorkingon.jobs.find(job => job.job._id === content._id);
    const isWorkingOn = true;
    this.router.navigateByUrl(`/request-details/${request._id};isWorkingOn=${isWorkingOn}`);
  }

  jobDetails(content) {
    console.log(content);
    this.router.navigateByUrl('/job-details/' + content._id);
  }
  
  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Sorry something went wrong!',
        },
      });
    }
  }
}
