import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageJobsComponent } from './manage-jobs/manage-jobs.component';
import { Routes, RouterModule } from '@angular/router';
import {MatTabsModule} from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { JobCardModule } from '../job-card/job-card.module';
import { NoDataModule } from 'src/app/shared/no-data/no-data.module';

const routes: Routes = [{ path: '', component: ManageJobsComponent }];

@NgModule({
  declarations: [ManageJobsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTabsModule,
    MatCardModule,
    MatIconModule,
    JobCardModule,
    NoDataModule
  ],
  exports: [ ManageJobsComponent ]
})
export class ManageJobsModule { }
