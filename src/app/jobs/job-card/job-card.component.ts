import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.scss'],
})
export class JobCardComponent implements OnInit {
  @Input() cardContent: any;

  imageToShow: any;
  image: any;
 
  userData: any;

  constructor() {}

  ngOnInit() {
    this.image = this.cardContent.imageUrl
    this.userData = {
      name: this.cardContent.creator.username,
      imageUrl: '',
      profile: false
    }
  }
  
}
