import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobCardComponent } from './job-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MonoAvatarModule } from 'src/app/shared/directives/mono-avatar/mono-avatar.module';

@NgModule({
  declarations: [JobCardComponent],
  imports: [CommonModule, MatCardModule, MatIconModule, MonoAvatarModule],
  exports: [JobCardComponent],
})
export class JobCardModule {}
