import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { JobChoiceComponent } from 'src/app/widgets/job-choice/job-choice.component';
import { FilterComponent } from 'src/app/widgets/filter-dialog/filter.component';
import { JOBS } from 'src/app/shared/exports/jobs-exports';
import { JobService } from 'src/app/services/job.service';
import { User } from 'src/app/shared/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { LoginDialogComponent } from 'src/app/widgets/login-dialog/login-dialog.component';
import { SnackbarComponent } from 'src/app/widgets/snackbar/snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

export const PRICES_TO_FILTER: any[] = [
  { label: 'Lower than 50', value: '50' },
  { label: 'Higher than 50', value: '75' },
];

export const FREELANCER_EMPLOYER_FILTER: any[] = [
  { label: 'Freelancer', value: true },
  { label: 'Employer', value: false },
];

@Component({
  selector: 'app-jobs-list',
  templateUrl: './jobs-list.component.html',
  styleUrls: ['./jobs-list.component.scss'],
})
export class JobsListComponent implements OnInit {
  @Input() jobIndex: any = null;

  jobs = JOBS;
  priceToFilter = PRICES_TO_FILTER;
  freelancerEmpolyerFilter = FREELANCER_EMPLOYER_FILTER;
  user: User;
  removable = true;
  isFreelancer: any;
  price: string;
  subCategory: any;
  title: string;
  specificJob: any;
  specificJobs = [];
  noJobs = {
    icon: 'work_off',
    text: 'No job found'
  }

  constructor(
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private jobServices: JobService,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    await this.checkIsFreelancer();
    this.loadJobData();
    this.getUser();
  }

  jobDetails(content) {
    this.router.navigateByUrl('/job-details/' + content._id);
  }

  clearPrice() {
    this.price = null;
    this.loadFilteredData();
  }

  clearCategory() {
    this.subCategory = null;
    this.loadFilteredData();
  }

  getUser() {
    this.authService.currentUser.subscribe((result) => {
      if (!result) {
        this.user = undefined;
      } else {
        this.user = result;
      }
    });
  }

  async addNewJob() {
    if (this.user) {
      this.router.navigateByUrl('/add-job');
    } else {
      await this.dialog
        .open(LoginDialogComponent)
        .afterClosed()
        .subscribe(() => {
          if (this.user) {
            this.router.navigateByUrl('/add-job');
          }
          return;
        });
    }
  }

  removeSpecificJob() {
    this.specificJob = null;
    localStorage.setItem('specificJobIndex', this.jobIndex);
    this.clearPrice();
    this.clearCategory();
  }

  async checkIsFreelancer() {
    this.isFreelancer = localStorage.getItem('isFreelancer');
    if (this.isFreelancer === null) {
      await this.dialog
        .open(JobChoiceComponent, { disableClose: true })
        .afterClosed()
        .subscribe((data) => {
          localStorage.setItem('isFreelancer', data.event);
          this.isFreelancer = data.event;
          this.loadFilteredData();
        });
    } else if (this.isFreelancer != null) {
      this.isFreelancer === 'true'
        ? (this.isFreelancer = true)
        : (this.isFreelancer = false);
    }
  }

  changeIsFreelancer(isFreelancer) {
    localStorage.setItem('isFreelancer', isFreelancer);
  }

  loadJobData() {
    this.jobIndex = localStorage.getItem('specificJobIndex');
    if (this.jobIndex != null) {
      this.specificJob = this.jobs[parseInt(this.jobIndex)].jobCategories;
      this.title = this.specificJob[0];
      this.loadFilteredData();
    } else if (this.jobIndex === null) {
      this.route.paramMap.subscribe(() => {
        this.jobIndex = parseInt(window.history.state.parameter);
        if (this.jobIndex != null && this.jobs[this.jobIndex] !== undefined) {
          this.specificJob = this.jobs[this.jobIndex].jobCategories;
          this.title = this.specificJob[0];
          localStorage.setItem('specificJobIndex', this.jobIndex);
        }
      });
    }
  }

  loadSubCategoriesData(jobIndex) {
    this.specificJob = this.jobs[parseInt(jobIndex)].jobCategories;
    this.title = this.specificJob[0];
    localStorage.setItem('specificJobIndex', jobIndex);
    this.loadFilteredData();
  }

  async loadFilteredData() {
    if(this.specificJob){
      const category = this.specificJob[1];
      await this.jobServices
        .getAllJobs(this.price, this.isFreelancer, this.subCategory, category)
        .then((res) => this.specificJobs = res.jobs)
        .catch(error => this.errorSnackBar(error));
    }

  }

  openFilterDialog(): void {
    this.dialog.open(FilterComponent, {
      data: {
        isFreelancer: this.isFreelancer,
        specificJob: this.specificJob[2],
        title: this.title,
        jobIndex: this.jobIndex
      } , panelClass: 'filter' 
    })
    .afterClosed()
    .subscribe(data => {
      if(data.specificJob && data.filters){
        const category = this.specificJob[1];
        this.jobServices.getAllJobs(data.filters.price, data.filters.isFreelancer, data.filters.category, category)
        .then(res => this.specificJobs = res.jobs)
        .catch(error => this.errorSnackBar(error));
      }else{
        this.specificJob = null;
      }
     
    });
  }

  errorSnackBar(error) {
    if (error.message) {
      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 3 * 1000,
        data: {
          isSuccess: true,
          message: 'Sorry something went wrong!',
        },
      });
    }
  }
}
