import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsListComponent } from './jobs-list.component';
import { Routes, RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { JobCategoriCardModule } from '../job-categori-card/job-categori-card.module';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { JobChoiceComponent } from 'src/app/widgets/job-choice/job-choice.component';
import { JobChoiceModule } from 'src/app/widgets/job-choice/job-choice.module';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { FilterComponent } from '../../widgets/filter-dialog/filter.component';
import { FilterModule } from '../../widgets/filter-dialog/filter.module';
import { JobCardModule } from '../job-card/job-card.module';
import { NoDataModule } from 'src/app/shared/no-data/no-data.module';

const routes: Routes = [{ path: '', component: JobsListComponent }];

@NgModule({
  declarations: [JobsListComponent],
  entryComponents: [JobChoiceComponent, FilterComponent],
  imports: [
    JobCardModule,
    CommonModule,
    MatCardModule,
    MatRadioModule,
    JobCategoriCardModule,
    FormsModule,
    MatSelectModule,
    MatButtonModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatChipsModule,
    JobChoiceModule,
    MatBottomSheetModule,
    FilterModule,
    NoDataModule
  ],
})
export class JobsListModule {}
