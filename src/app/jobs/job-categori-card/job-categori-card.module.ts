import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobCategoriCardComponent } from './job-categori-card.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [JobCategoriCardComponent],
  imports: [CommonModule, MatCardModule],
  exports: [JobCategoriCardComponent],
})
export class JobCategoriCardModule {}
