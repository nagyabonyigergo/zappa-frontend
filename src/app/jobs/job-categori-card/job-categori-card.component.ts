import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-job-categori-card',
  templateUrl: './job-categori-card.component.html',
  styleUrls: ['./job-categori-card.component.scss'],
})
export class JobCategoriCardComponent implements OnInit {
  @Input() cardContent: any;

  constructor() {}

  ngOnInit() {}
}
