import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCategoriCardComponent } from './job-categori-card.component';

describe('JobCategoriCardComponent', () => {
  let component: JobCategoriCardComponent;
  let fixture: ComponentFixture<JobCategoriCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobCategoriCardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobCategoriCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
